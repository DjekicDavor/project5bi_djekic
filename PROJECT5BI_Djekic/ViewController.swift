//
//  ViewController.swift
//  PROJECT5BI_Djekic
//
//  Created by Studente on 22/11/2018.
//  Copyright © 2018 ITT Barsanti. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet var binaryNumber: UITextField!
    
    @IBAction func insertZerosOnes(_ sender: Any) {
        //To-Do: check if only 0s and 1s have been inserted dynamically
    }
    
    @IBOutlet var ipAddress: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    @IBAction func convertBtn(_ sender: Any) {
        let binAddress : String = binaryNumber.text!
        if checkInputLength(inputString: binAddress) {
            printDecimalAddress(binString : binAddress)
        }
        else {
            ipAddress.text = "Insert a valid binary number"
        }
    }
    
    func checkInputLength(inputString : String) -> Bool {
        return (inputString.count == 32)
    }
    
    func printDecimalAddress(binString : String) {
        var decAddress : String = ""
        for n in 0...3 {
            //Get the 8 binary numbers
            let c = binString.characters;
            let temp = c.index(c.startIndex, offsetBy: n*8)..<c.index(c.endIndex, offsetBy: -((3-n)*8))
            let substring = binString[temp]
            
            //Convert into decimal and adding to the container string
            decAddress += String(fromStringBinToInt(binNumber: String(substring)))
            decAddress.append(".")
        }
        decAddress.remove(at: decAddress.index(before: decAddress.endIndex))
        ipAddress.text = decAddress
    }

    func fromStringBinToInt(binNumber : String) -> Int {
        return Int(binNumber, radix: 2)!
    }
}

